; Home
G28



; cycle 1
; front of the X tower, with the delta arms to the X Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the X tower end stop error.
G0 Z10 X-104 Y-60 F10000
G0 Z2 F1000

; front of the Y tower, with the delta arms to the Y Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the Y tower end stop error.
G0 Z10 F10000
G0 X104 Y-60
G0 Z2 F1000

; front of the Z tower, with the delta arms to the Z Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the Z tower end stop error.
G0 Z10 F10000
G0 X0 Y120
G0 Z2 F1000
