; Home
G28

; Go to a point centered on the bed, at a Z position
; of 2.0. For testing the horizontal radius error.
G0 Z10 F10000
G0 X0 Y0
GO Z2 F1000


;go to a point right in front
; of the X tower, with the delta arms to the X Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the X tower end stop error.

G0 Z10 X-104 Y-60 F10000
G0 Z2 F1000



; Go to a point right in front
; of the Y tower, with the delta arms to the Y Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the Y tower end stop error.
G0 Z10 F10000
G0 X104 Y-60
G0 Z2 F1000

; Go to a point right in front
; of the Z tower, with the delta arms to the Z Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the Z tower end stop error.
G0 Z10 F10000
G0 X0 Y120
G0 Z2 F1000


; Go to a point centered on the bed, at a Z position
; of 2.0. For testing the horizontal radius error.
G0 Z10 F10000
G0 X0 Y0
GO Z2 F1000
