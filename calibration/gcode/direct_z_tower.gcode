



; Go to a point right in front
; of the Z tower, with the delta arms to the Z Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the Z tower end stop error.
G0 Z10 F10000
G0 X0 Y120
G0 Z2 F1000
