; Go to a point right in front
; of the Y tower, with the delta arms to the Y Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the Y tower end stop error.
G0 Z10 F10000
G0 X104 Y-60
G0 Z2 F1000
