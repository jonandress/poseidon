from PIL import Image
import glob, os
from decimal import *
import sys

defaultInputImage  = "experiment.jpg"
defaultOutPutWidthInMM  = 152
defaultLpPerMm  = 4
defaultDwell  = 20
defaultSpeed  = 3000
defaultPower  = 255
defaultOutputToTerm  = ""
defaultPathToOutputFile  = "/Volumes/jonandress/Desktop/Output.txt"





print ("\n********************************************************* \n")
print ("Enter input Parameters to parse image into machine code \n")
print ("********************************************************* \n")
inputImage = str(input("Image to parse: ["+defaultInputImage+"]") or "./"+defaultInputImage)
outPutWidthInMM = int(input("resize to width in mm: ["+str(defaultOutPutWidthInMM)+"]") or defaultOutPutWidthInMM);
print(str(outPutWidthInMM)+"  mm is "+str(outPutWidthInMM/25.4)+" inches")
# inchesToMm = 25.4 # 6 inches would be 152.4
lpPerMm = int(input("laser pixels per mm: ["+str(defaultLpPerMm)+"]") or defaultLpPerMm)
dwell = str(input("dwell in milliseconds: ["+str(defaultDwell)+"]") or defaultDwell)
speed = int(input("speed: ["+str(defaultSpeed)+"]") or defaultSpeed )
power = int(input("power: ["+str(defaultPower)+"]") or defaultPower)
pathToOutputFile = str(input("pathToOutputFile ["+str(defaultPathToOutputFile)+"]") or defaultPathToOutputFile)
#  print("dwell: "+str(dwell));

print("\n")
im = Image.open(inputImage);
gray = im.convert('L');
gray_w, gray_h = gray.size;

resizeRatio = (outPutWidthInMM*lpPerMm) / gray_w


# gray.show();
newimage = Image.new(mode="L", size=(int(float(gray_w*resizeRatio)), int(float(gray_h*resizeRatio))))
w2, h2 = newimage.size;
#
print("\n*********************************************************\n");
# print(gray_w, gray_h)
print("new image size: ",w2, h2)

px = 0;
py = 0;


str1="G28\n";
str1+="G1 Z110 F9000\n" #drop to print position 110 mm off of bed.
# print(w2, h2)
ox = Decimal(((w2/lpPerMm)/2)*-1);
oy = Decimal(((h2/lpPerMm)/2)*-1);
# Decimal((h2)*-1);
increment = Decimal(1 / lpPerMm)  #.125
_y = oy;
_x = ox;
print("starting values increment:"+str(increment)+", ox: "+str(ox)+", oy: "+str(oy))
print("range: "+str(h2)+", "+str(w2))

for y in range(h2):
    py = Decimal(y/h2);
    sy = int(float(py * gray_h));
    _y += increment;
    _x = ox;
    for x in range(w2):
        px = Decimal(x/w2);
        sx = int(float(px * gray_w));
        p = gray.getpixel((sx,sy));
        newimage.putpixel((x,y),(p));
        _x += increment;
        if p < 128:
            str1+="G1 X"+str(_x)+" Y"+str( _y )+" F"+str(speed)+"\n"; #move to position.
            str1+="M106 S"+str(power)+" \n"
            str1+="G4 P"+str(dwell)+" \n"
            str1+="M107\n"

str1+="G28 \n"
text_file = open(pathToOutputFile, "w")
text_file.write(str1);
text_file.close();
print("x ",x,", y ",y, ", _x ",_x,", _y ",_y)
print("lpPerMm: "+str(lpPerMm)+", dwell:"+str(dwell)+", power: "+str(power)+", speed: "+str(speed)+"\n"+"outPutWidthInMM: " +str(outPutWidthInMM)+", dimension in mm: " +str(outPutWidthInMM));

print("\nDone!\n");
print("*********************************************************\n");

# newimage.show();
