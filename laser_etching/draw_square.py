dimenInches = str(input("Square dimension: [1]") or 1)
print(dimenInches)
dimension  = round((25.4*int(dimenInches)), 2)



speed = str(input("speed: [1000]") or "1000")
power = str(input("power: [255]") or "255")
dwell = str(input("dwell in ms: [1000]") or "1000")
pathToOutputFile = str(input("pathToOutputFile [/Volumes/jonandress/Desktop/square_gcode.txt]") or "/Volumes/jonandress/Desktop/square_gcode.txt")
hd = dimension / 2
nhd = str(hd *-1)
hd = str(hd);
str1="G28\n";
str1+="G1 Z110 X0 Y0 F9000\n" #drop to print position 110 mm off of bed.

str1+="M106 S"+str(power)+" \n"
str1+="G4 P"+str(dwell)+" \n"
str1+="M107\n"

str1+="G1 X"+nhd+" Y"+nhd+" F9000\n"  #corner 1
str1+="M106 S"+str(power)+" \n"
str1+="G1 X"+hd+" Y"+nhd+" F"+speed+"\n" #corner 2
str1+="G1 X"+hd+" Y"+hd+"  F"+speed+"\n" #corner 3
str1+="G1 X"+nhd+" Y"+hd+"  F"+speed+"\n" #corner 4
str1+="G1 X"+nhd+" Y"+nhd+"  F"+speed+"\n" #corner 1
str1+="M107\n" # laser off
str1+="G28\n";


text_file = open(pathToOutputFile, "w")
text_file.write(str1)
text_file.close()
