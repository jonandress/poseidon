
  G26 PRINT TEST GRID.

  G1 Z0 F20000 - DIRECT MOVE TO Z 0
 * M500 - Store parameters in EEPROM. (Requires EEPROM_SETTINGS)
 * M501 - Restore parameters from EEPROM. (Requires EEPROM_SETTINGS)
 * M502 - Revert to the default "factory settings". ** Does not write them to EEPROM! **
 * M503 - Print the current settings (in memory): "M503 S<verbose>". S0 specifies compact output.



M665 H572.5  - sets the height of the printer.  you must then save it to eeprom via M500.







* M665 - Set delta configurations: "M665 L<diagonal rod> R<delta radius> S<segments/s> A<rod A trim mm> B<rod B trim mm> C<rod C trim mm> I<tower A trim angle> J<tower B trim angle> K<tower C trim angle>" (Requires DELTA)


 M665 - Delta Configuration
     Argument	Description
     [B<linear>]
     Delta calibration radius

     [H<linear>]
     Delta height

     [L<linear>]
     Diagonal rod

     [R<linear>]
     Delta radius

     [S<float>]
     Segments per second

     [X<float>]
     Alpha (Tower 1) angle trim

     [Y<float>]
     Beta (Tower 2) angle trim

     [Z<float>]
     Gamma (Tower 3) angle trim
