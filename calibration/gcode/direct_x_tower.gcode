; Home
;G28
;go to a point right in front
; of the X tower, with the delta arms to the X Cheapskate
; vertical, and at a Z position of 2.0. For measuring
; the X tower end stop error.

G0 Z10 X-104 Y-60 F10000
G0 Z2 F1000
